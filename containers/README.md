You can either do this demo on a linux machine or using a Docker Container.

For Linux: Run container.sh and container-alpine.sh and play around

For Docker:
1. Build your Image using `docker build -t container-demo:latest .`
2. Run a Container with `docker run -i -t --privileged container-demo /bin/sh`
3. Play with it: `unshare --mount --uts --ipc --net --pid --fork` `unshare --mount --uts --ipc --net --pid --fork chroot alpine-fs busybox ash`
